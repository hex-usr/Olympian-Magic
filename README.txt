Olympian Magic
Author: hex_usr
License: General Public License version 3

Olympian Magic is a tool for converting SNES gamepaks (cartridge folders) from
bsnes v073 format, higan v092 format, and higan v096 format to higan/bsnes v107
format. It translates manifests and renames program ROMs according to what
higan/bsnes v107 expects. If the gamepak has a firmware for the uPD7725,
Olympian Magic will split the firmware into separate program and data ROMs and
reencode them from big endian to little endian.

higan/bsnes v107 is the only supported target format. The higan v096 format is
deprecated, and all previous formats are obsolete, so they are not supported as
target formats.

higan/bsnes is licensed under the General Public License version 3. Since
Olympian Magic converts manifests for this emulator, it uses the same license.