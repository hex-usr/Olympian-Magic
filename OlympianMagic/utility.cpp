auto GamePak::compressAddress(string address) -> string {
  auto parts = address.split(":");
  for(string& part : parts) {
    auto subpart = part.split(",");
    for(uint lo = 0; lo < subpart.size() - 1; lo++) {
      uint hi = lo + 1;
      while(hi < subpart.size()) {
        auto part0 = subpart[lo].split("-");
        auto part1 = subpart[hi].split("-");
        if(part0[1].hex() == part1[0].hex() - 1) {  //left hi connected to right lo
          part0[1] = part1[1];
          subpart[lo] = part0.merge("-");
          subpart.remove(hi);
        } else if(part0[0].hex() == part1[1].hex() + 1) {  //left lo connected to right hi
          part0[0] = part1[0];
          subpart[lo] = part0.merge("-");
          subpart.remove(hi);
        } else {
          hi++;
        }
      }
    }
    part = subpart.merge(",");
  }
  return parts.merge(":");
}

auto GamePak::mergeMap(const Markup::Node& map0, const Markup::Node& map1) -> maybe<Markup::Node> {
  if(map0.size() != map1.size()) return nothing;
  if(map0.value() != map1.value()) return nothing;  //distinguish "mcu" maps
  for(auto node : map0) {
    if(node.name() == "address") continue;
    if(node.value() != map1[node.name()].value()) return nothing;
  }
  string address = map0["address"].value();
  auto part0 = address.split(":");
  auto part1 = map1["address"].value().split(":");
  for(uint i : {0, 1}) {  //0: bank, 1: addr
    if(part0(i) != part1(i) && part0(1 - i) == part1(1 - i)) {
      part0(i).append(",", part1(i));
      part0(i) = part0(i).split(",").isort().merge(",");
      address = part0.merge(":");
    }
  }
  if(address != map0["address"].value()) {
    auto map = map0.clone();
    map["address"].setValue(compressAddress(address));
    return map;
  }
  return nothing;
}

auto GamePak::mergeMaps(vector<Markup::Node> maps) -> vector<Markup::Node> {
  for(uint lo = 0; lo < maps.size() - 1; lo++) {
    uint hi = lo + 1;
    while(hi < maps.size()) {
      if(auto map = mergeMap(maps[lo], maps[hi])) {
        maps[lo] = Markup::Node{};
        maps[lo].copy(map());
        maps.remove(hi);
      } else {
        hi++;
      }
    }
  }
  return maps;
}
