#include "program.hpp"

unique_pointer<Program> program;

ManifestTab::ManifestTab(TabFrame* parent) : TabFrameItem(parent) {
  setText("Manifest");
  layout.setMargin(5);

  inManifestEdit.setFont(Font().setFamily(Font::Mono)).setWordWrap(false);
  outManifestEdit.setFont(Font().setFamily(Font::Mono)).setWordWrap(false);

  inManifestEdit.onChange([&]() -> void {
    program->gamepak.setMarkup(inManifestEdit.text());
    program->convert();
  });

  /*
  deleteButton.setText("Delete manifest").setEnabled(false).onActivate([&]() -> void {
    string path = program->gamepak.path;
    string answer = MessageDialog().setParent(*program).setTitle("Olympian Magic").setText({
      "Are you sure you want to delete the manifest at\n\"", path, "\"?"
    }).question();
    if(answer == "Yes") {
      path.transform("\\", "/");
      path.trimRight("/").append("/");
      file::remove({path, "manifest.bml"});
      file::remove({path, Location::prefix(path), ".xml"});
      deleteButton.setEnabled(false);
    }
  });
  */
}

MemoryTab::MemoryTab(TabFrame* parent) : TabFrameItem(parent) {
  setText("Memories");
  layout.setMargin(5);
}

auto MemoryTab::refresh(vector<GamePak::Memory> newMemoryList) -> void {
  static auto setMemories = [&](TableView& table, const vector<GamePak::Memory>& memoryList) -> void {
    table.reset();
    table.append(TableViewHeader().setVisible()
      .append(TableViewColumn().setText("Type"))
      .append(TableViewColumn().setText("Size").setWidth(60).setAlignment(1.0))
      .append(TableViewColumn().setText("Content"))
      .append(TableViewColumn().setText("Arch.").setWidth(60))
      .append(TableViewColumn().setText("Id.").setWidth(40))
      .append(TableViewColumn().setText("Depth").setAlignment(1.0))
      .append(TableViewColumn().setText("Endian"))
      .append(TableViewColumn().setText("Offset").setAlignment(1.0))
      .append(TableViewColumn().setText("Filename").setExpandable())
    );
    for(const auto& memory : memoryList) {
      if((memory.type() == "RAM" || memory.type() == "RTC") && memory.isVolatile()) continue;
      table.append(TableViewItem()
        .append(TableViewCell().setText(memory.type()))
        .append(TableViewCell().setText({"0x", hex(memory.size())}))
        .append(TableViewCell().setText(memory.content()))
        .append(TableViewCell().setText(memory.architecture()))
        .append(TableViewCell().setText(memory.identifier()))
        .append(TableViewCell().setText(memory.depth()))
        .append(TableViewCell().setText(
          memory.endian() == Endian::MSB ? "MSB" :
          memory.endian() == Endian::LSB ? "LSB" :
          ""))
        .append(TableViewCell().setText({"0x", hex(memory.offset())}))
        .append(TableViewCell().setText(memory.filename()))
      );
    }
  };

  setMemories(inTable, program->gamepak.memoryList);
  setMemories(outTable, newMemoryList);
}

Program::Program(const string in) {
  program = this;
  Application::onMain({&Program::main, this});

  layout.setMargin(5);
  setTitle("Olympian Magic");
  setResizable(true);

  loadPath.setText(in).onChange([&]() -> void {
    string path = loadPath.text();
    load(path);
  });

  loadButton.setText("Load ...").onActivate([&]() -> void {
    if(string path = BrowserDialog()
      .setFilters({"Super Famicom|*.sfc"})
      .setPath(settings["last-path"].text())
      .openFolder()
    ) {
      loadPath.setText(path);
      settings("last-path").setValue(Location::dir(path));
      loadPath.doChange();
    }
  });

  /*
  #if defined(Hiro_ComboEdit)
    #define versionRange(low, high) \
      for(uint v = low; v <= high; v++) \
        outManifestVersion.append(ComboEditItem().setText({"v", pad(v, 3L, '0')}))
    versionRange( 73,  73);
    versionRange( 92,  95);
    versionRange( 96, 106);
    versionRange(107, 107);
    #undef versionRange
  #endif

  outManifestVersion.setText("v107").onChange({&Program::convert, &*program});
  */

  saveButton.setText("Save gamepak").setEnabled(false).onActivate([&]() -> void {
    string path = loadPath.text();
    string answer = MessageDialog().setParent(*this).setTitle("Olympian Magic").setText({
      "Are you sure you want to overwrite the gamepak at\n\"", path, "\"?"
    }).question();
    if(answer == "Yes") {
      save(path);
    }
  });

  onClose({&Program::quit, this});

  settings = BML::unserialize(file::read(locate("settings.bml")));

  loadPath.doChange();

  setSize({1024, 350});
  setVisible(true);
}

auto Program::load(string path) -> void {
//manifestTab.deleteButton.setEnabled(false);
  path.transform("\\", "/");
  if(path.endsWith(".sfc") && directory::exists(path)) path.append("/");
  if(path.endsWith(".sfc/") && directory::exists(path)) {
  //if(execute("icarus", "--name").output.strip() != "icarus") {
  //  manifestTab.statusLabel.setText("Could not find icarus!");
  //  return;
  //}
    gamepak.load(path);
    string markup{gamepak.markup};
    if(!markup) {
    //manifestTab.statusLabel.setText("This gamepak has no manifest. The manifest above was generated by icarus.");
      manifestTab.statusLabel.setText("This gamepak has no manifest.");
      return;
    }
    bool legacyFilenames;
    if(auto memory = gamepak.memory("program.rom")) {
      legacyFilenames = memory().filename().endsWith(".sfc");
    } else {
      manifestTab.statusLabel.setText("This gamepak has no program ROM and is invalid.");
      return;
    }
  //if(legacyFilenames) file::rename({path, Location::prefix(path), ".sfc"}, {path, "program.rom"});
  //auto icarus = execute("icarus", "--manifest", path);
  //if(legacyFilenames) file::rename({path, "program.rom"}, {path, Location::prefix(path), ".sfc"});

    if(!gamepak.icarus) {
      manifestTab.statusLabel.setText("This gamepak has a manifest.");
      /*
      string icarusMarkup = icarus ? icarus.output : "";
      if(icarus && icarusMarkup) {
        if(gamepak.compare(icarusMarkup)) {
        //manifestTab.deleteButton.setEnabled(true);
          manifestTab.statusLabel.setText("This manifest matches icarus's generated manifest and can be safely deleted.");
        } else {
          manifestTab.statusLabel.setText("This manifest does not match icarus's generated manifest.");
        }
        if(icarusMarkup.endsWith("<")) {
          markup.append("\n<!-- icarus:\n", icarusMarkup, "-->");
        } else {
          icarusMarkup.replace("\n", "\n  :").prepend("icarus\n  :");
          if(!markup.endsWith("\n")) markup.append("\n");
          markup.append("\n", icarusMarkup);
        }
      } else if(icarus) {
        manifestTab.statusLabel.setText("icarus could not generate a manifest to compare this one with.");
      } else {
        manifestTab.statusLabel.setText("This gamepak has a manifest, but icarus cannot be found!");
      }
      */
    }
    manifestTab.inManifestEdit.setText(markup);
    manifestTab.inManifestEdit.doChange();
  } else {
    manifestTab.statusLabel.setText("Gamepak not found.");
  }
}

auto Program::save(string path) -> void {
  uint outVersion = 107;//outManifestVersion.text().trimLeft("v").natural();

  path.transform("\\", "/");
  path.trimRight("/").append("/");
  directory::create(path);

  string oldPath = path;
  bool overwrite = oldPath == path;

  if(!gamepak.icarus) {
    string markup = manifestTab.outManifestEdit.text();
    file::write({path, "manifest.bml"}, markup);
    manifestTab.inManifestEdit.setText(markup);
  }

  if(overwrite && outVersion >= 92) {
    file::remove({oldPath, Location::prefix(path), ".xml"});
  }

  vector<uint8_t> data;
  for(auto memory : gamepak.memoryList) {
    if((memory.type() == "RAM" || memory.type() == "RTC") && memory.isVolatile()) continue;
    auto newMemory = gamepak.convertMemory(outVersion, memory);
    bool renameOnly = (
       newMemory.depth()  == memory.depth()
    && newMemory.endian() == memory.endian()
    && newMemory.offset() == memory.offset()
    );

    if(renameOnly && newMemory.filename() == memory.filename()) continue;

    if(file::exists({path, newMemory.filename()})) {
      string answer = MessageDialog().setTitle("Olympian Magic").setParent(*this).setText({
        "A file with the name ", newMemory.filename(), " already exists.\n"
        "If you rename it, its new name will be \"", newMemory.filename(), ".old\"."
      }).question({"Delete", "Rename"});
      if(answer == "Delete") {
        file::remove({path, newMemory.filename()});
      } else {
        file::rename({path, newMemory.filename()}, {path, newMemory.filename(), ".old"});
      }
    }
    if(renameOnly) {
      if(overwrite) {
        file::rename({oldPath, memory.filename()}, {path, newMemory.filename()});
      } else {
        file::copy({oldPath, memory.filename()}, {path, newMemory.filename()});
      }
    } else {
      file fp;
      uintmax word;

      data.reset();
      if(fp.open({oldPath, memory.filename()}, file::mode::read)) {
        data.reserve(memory.size());
        fp.seek(memory.offset());
        for(uint w : range(memory.size() / memory.depth())) {
          if(memory.endian() != Endian::MSB) word = fp.readl(memory.depth());
          if(memory.endian() == Endian::MSB) word = fp.readm(memory.depth());
          for(uint b : range(memory.depth())) {
            data.append(word >> (b << 3) & 0xff);
          }
        }
      }
      fp.close();

      if(fp.open({path, newMemory.filename()}, file::mode::write)) {
        for(uint w : range(newMemory.size() / newMemory.depth())) {
          word = 0;
          for(uint b : range(newMemory.depth())) {
            word |= data[w * newMemory.depth() + b] << (b << 3);
          }
          if(newMemory.endian() != Endian::MSB) fp.writel(word, newMemory.depth());
          if(newMemory.endian() == Endian::MSB) fp.writem(word, newMemory.depth());
        }
      }
      fp.close();

      if(overwrite && newMemory.filename() != memory.filename()) {
        if(memory.offset() + memory.size() == file::size({path, memory.filename()})) {
          file::remove({path, memory.filename()});
        }
      }
    }
  }

  if(overwrite && outVersion == 107) {
    directory::create({path, "msu1/"});
    string oldName;

    oldName = gamepak.msu1DataName();
    file::move({oldPath, oldName}, {path, "msu1/data.rom"});

    for(uint trackID : range(65536)) {
      oldName = gamepak.msu1TrackName(trackID);
      file::move({oldPath, oldName}, {path, "msu1/track-", trackID, ".pcm"});
    }
  }
}

auto Program::convert() -> void {
  uint outVersion = 107;//outManifestVersion.text().trimLeft("v").natural();

  string newMarkup = gamepak.convertManifest(outVersion);

  manifestTab.outManifestEdit.setText(newMarkup);
  saveButton.setEnabled(!!newMarkup);
  if(newMarkup) {
    memoryTab.refresh(gamepak.convertMemories(outVersion));
  }
}

auto Program::main() -> void {
  usleep(2000);
}

auto Program::quit() -> void {
  file::write(locate("settings.bml"), BML::serialize(settings));
  setVisible(false);
  Application::quit();
}

auto locate(string name) -> string {
  string location = {Path::program(), name};
  if(inode::exists(location)) return location;

  location = {Path::config(), "OlympianMagic/", name};
  if(inode::exists(location)) return location;

  directory::create({Path::local(), "OlympianMagic/"});
  return {Path::local(), "OlympianMagic/", name};
}

#include <nall/main.hpp>
auto nall::main(string_vector args) -> void {
  args.takeLeft();  //ignore program location in argument parsing
  uint targetVersion = 0;
  string in = "";
  bool test = false;
  while(args) {
    string arg = args.takeLeft();
    if(!arg.beginsWith("--")) {
      in = arg;
    }
    if(arg == "--test") {
      test = true;
    }
    if(arg == "--target-version") {
      targetVersion = args.takeLeft().trimLeft("v").natural();
    }
  }

  if(targetVersion) {
    GamePak gamepak;
    if(test) {
      string contents = file::read(in);
      auto all = BML::unserialize(contents);
      for(auto manifest : all.find("manifest")) {
        gamepak.setMarkup(manifest.text());
        string converted = gamepak.convertManifest(targetVersion);
        print("manifest\n");
        for(auto line : converted.trimRight("\n").split("\n")) {
          print("  :", line, "\n");
        }
        print("\n");
      }
      return;
    }
    if(directory::exists(in)) {
      in.trimRight("/").append("/");
      string manifestPath;
      manifestPath = {in, "manifest.bml"};
      if(file::exists(manifestPath)) gamepak.setMarkup(file::read(manifestPath));
      manifestPath = {in, Location::prefix(in), ".xml"};
      if(file::exists(manifestPath)) gamepak.setMarkup(file::read(manifestPath));
    } else if(file::exists(in)) {
      gamepak.setMarkup(file::read(in));
    }
    print(gamepak.convertManifest(targetVersion));
    return;
  }

  Application::setName("Olympian Magic");
  new Program(in);
  Application::run();
}
