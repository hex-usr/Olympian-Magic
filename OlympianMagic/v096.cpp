auto GamePak::load_v096() -> void {
  auto information = document["information"];
  sha256   = information["sha256"].text();
  label    = information["title"].text();
  name     = information["name"].text();
  if(information["serial"]) {
    region = information["serial"].text();
  } else if(information["region"]) {
    region = information["region"].text();
  }
  revision = information["revision"].text();
  board    = information["board"].text();

  auto boardNode = document["board"];

  auto loadMemoryFromNode = [&](string query, string type, string content,
    string manufacturer = "", string architecture = "", string identifier = ""
  ) -> void {
    auto node = boardNode[query];
    if(!node) return;
    Memory memory;
    memory.type(type);
    if(type == "RAM" || type == "RTC") memory.setVolatile((bool)node["volatile"]);
    memory.size(node["size"].natural());
    memory.content(content);
    memory.manufacturer(manufacturer);
    memory.architecture(architecture);
    if(manufacturer && architecture && !identifier) {
      identifier = node["name"].text().split(".").left().upcase();
      if(identifier == "CX4") identifier = "Cx4";
    }
    if(identifier) memory.identifier(identifier);
    if(manufacturer == "ARM")     memory.depth(content == "Program" || type == "RAM" ? 4 : 1).endian(Endian::LSB);
    if(manufacturer == "Hitachi") memory.depth(3                                            ).endian(Endian::LSB);
    if(manufacturer == "NEC")     memory.depth(content == "Program" ? 3 : 2                 ).endian(Endian::LSB);
    memory.filename(node["name"].text());
    memoryList.append(memory);
  };

  auto loadOscillatorFromNode = [&](string query) -> void {
    auto node = boardNode[query];
    if(!node) return;

    uint frequency = node["frequency"].natural();

    if(query == "necdsp" && frequency == 8'000'000) frequency = 7'600'000;

    if(!frequency) return;
    Oscillator oscillator;
    oscillator.frequency(frequency);
    oscillatorList.append(oscillator);
  };

  loadMemoryFromNode("sa1/rom",   "ROM", "Program");
  loadMemoryFromNode("sa1/bwram", "RAM", "Save");
  loadMemoryFromNode("sa1/iram",  "RAM", "Internal");

  loadMemoryFromNode("superfx/rom", "ROM", "Program");
  loadMemoryFromNode("superfx/ram", "RAM", "Save");

  loadMemoryFromNode("hitachidsp/rom",  "ROM", "Program");
  loadMemoryFromNode("hitachidsp/ram",  "RAM", "Save");

  loadMemoryFromNode("spc7110/prom", "ROM", "Program");
  loadMemoryFromNode("spc7110/drom", "ROM", "Data");
  loadMemoryFromNode("spc7110/ram",  "RAM", "Save");

  loadMemoryFromNode("sdd1/rom", "ROM", "Program");
  loadMemoryFromNode("sdd1/ram", "RAM", "Save");

  loadMemoryFromNode("obc1/ram", "RAM", "Save");

  loadMemoryFromNode("rom", "ROM", memory("program.rom") ? "Expansion" : "Program");
  loadMemoryFromNode("ram", "RAM", "Save");

  bool sgb1 = !boardNode["icd2/frequency"];
  loadMemoryFromNode("icd2/rom", "ROM", "Boot", "Nintendo", sgb1 ? "DMG" : "MGB", sgb1 ? "SGB1" : "SGB2");

  loadMemoryFromNode("mcc/ram", "RAM", "Download");

  loadMemoryFromNode("armdsp/prom", "ROM", "Program", "SETA", "ARM6");
  loadMemoryFromNode("armdsp/drom", "ROM", "Data",    "SETA", "ARM6");
  loadMemoryFromNode("armdsp/ram",  "RAM", "Data",    "SETA", "ARM6");

  loadMemoryFromNode("hitachidsp/drom", "ROM", "Data", "Hitachi", "HG51BS169");
  loadMemoryFromNode("hitachidsp/dram", "RAM", "Data", "Hitachi", "HG51BS169");

  string identifier = boardNode["necdsp/prom/name"].text().split(".").left().upcase();
  loadMemoryFromNode("necdsp/prom", "ROM", "Program", "NEC", boardNode["necdsp/model"].text());
  loadMemoryFromNode("necdsp/drom", "ROM", "Data",    "NEC", boardNode["necdsp/model"].text());
  loadMemoryFromNode("necdsp/dram", "RAM", "Data",    "NEC", boardNode["necdsp/model"].text(), identifier);

  loadMemoryFromNode("epsonrtc/ram", "RTC", "Time", "Epson", "RTC4513");

  loadMemoryFromNode("sharprtc/ram", "RTC", "Time", "Sharp", "SRTC");

  if(boardNode["superfx"]) {
    if(!boardNode["superfx/revision"] || boardNode["superfx/revision"].natural() >= 2) {
      oscillatorList.append(Oscillator{}.frequency(21440000));
    }
  }

  loadOscillatorFromNode("armdsp");
  loadOscillatorFromNode("hitachidsp");
  loadOscillatorFromNode("necdsp");
}

auto GamePak::convertManifest_v073_v096(uint targetVersion) -> string {
  auto oldCartridge = document["cartridge"];
  uint romSize = 0;
  if(auto programROM = memory("program.rom")) {
    romSize = programROM().size();
  }

  string newManifest = "board";
  if(oldCartridge["region"]) newManifest.append(" region=", oldCartridge["region"].text().downcase());
  newManifest.append("\n");

  auto convertROM = [&](string query, uint indent, string nodeName, string romName, uint romSize) -> void {
    if(auto rom = oldCartridge[query]) {
      newManifest.append(string::repeat(" ", indent));
      newManifest.append(nodeName, " name=", romName, " size=0x", hex(romSize), "\n");
    }
  };

  auto convertRAM = [&](string query, uint indent, string nodeName, string ramName, bool isVolatile) -> void {
    if(auto ram = oldCartridge[query]) {
      newManifest.append(string::repeat(" ", indent));
      //hexadecimal is enforced even without a "0x" prefix
      newManifest.append(nodeName, " name=", ramName, " size=0x", hex(ram["size"].text().hex()));
      if(isVolatile) newManifest.append(" volatile");
      newManifest.append("\n");
    }
  };

  auto convertMap = [&](Markup::Node map, uint indent, string suffix = "") -> void {
    newManifest.append(string::repeat(" ", indent));
    newManifest.append("map", suffix, " address=", map["address"].text());
    uint size = map["size"].text().hex();
    uint base = map["offset"].text().hex();
    uint mask = 0;
    if(size) newManifest.append(" size=0x", hex(size));
    if(base) newManifest.append(" base=0x", hex(base));
    if(map["mask"]) {  //Not supported by bsnes v073, but enabled for overriding the Direct/Linear/Shadow modes
      mask = map["mask"].natural();
    } else {
      string mode = "direct";
      if(map["mode"]) mode = map["mode"].text();
      if(mode == "linear") {
        auto parts = map["address"].text().split(":")[1].split("-");
        mask = ~(parts(0).hex() ^ parts(1).hex()) & 0xffff;
      }
    }
    if(mask) {
      //mask is unnecessary if the size attribute ensures an appropriate mask anyway
      if(map["size"].natural() != 0x10000 - mask) newManifest.append(" mask=0x", hex(mask));
    }
    newManifest.append("\n");
  };

  auto convertMaps = [&](string query, uint indent, string suffix = "") -> void {
    auto maps = oldCartridge.find(query);
    if(!maps) return;
    if(suffix) suffix.prepend("=");
    for(auto map : mergeMaps(maps)) convertMap(map, indent, suffix);
  };

  auto calcMask = [&](Markup::Node map0, Markup::Node map1) -> uint {
    auto addr0 = map0["address"].text().split(":")[1].split("-");
    auto addr1 = map1["address"].text().split(":")[1].split("-");
    uint mask0 = addr0[0].hex() ^ addr0[1].hex();
    uint mask1 = addr1[0].hex() ^ addr1[1].hex();
    if(mask0 != mask1) return 0;
    return mask0;
  };

  if(oldCartridge["rom"]) {
    convertROM("rom", 2, "rom", "program.rom", romSize);
      convertMaps("rom/map", 4);
  }

  if(oldCartridge["ram"]) {
    convertRAM("ram", 2, "ram", "save.ram", false);
      convertMaps("ram/map", 4);
  }

  if(auto sa1 = oldCartridge["sa1"]) {
    newManifest.append("  sa1\n");
      convertMaps("sa1/mmio/map", 4);
      convertROM("sa1/rom", 4, "rom", "program.rom", romSize);
        for(auto map : sa1.find("rom/map")) {
          if(map["address"].text() == "00-3f:8000-ffff"
          || map["address"].text() == "80-bf:8000-ffff") {
            map.append(Markup::Node("mask", "0x408000"));
          }
        }
        convertMaps("sa1/rom/map", 6);
      convertRAM("sa1/bwram", 4, "bwram", "save.ram", false);
        for(auto map : sa1.find("bwram/map")) {
          if(map["address"].text() == "00-3f:6000-7fff"
          || map["address"].text() == "80-bf:6000-7fff") {
            if(!map["size"]) map.append(Markup::Node("size", "0x2000"));
          }
        }
        convertMaps("sa1/bwram/map", 6);
      convertRAM("sa1/iram", 4, "iram", "internal.ram", true);
        for(auto map : sa1.find("iram/map")) {
          if(map["address"].text() == "00-3f:3000-37ff"
          || map["address"].text() == "80-bf:3000-37ff") {
            if(!map["size"]) map.append(Markup::Node("size", "0x800"));
          }
        }
        convertMaps("sa1/iram/map", 6);
  }

  if(auto superfx = oldCartridge["superfx"]) {
    newManifest.append("  superfx\n");
      convertMaps("superfx/mmio/map", 4);
      convertROM("superfx/rom", 4, "rom", "program.rom", romSize);
        convertMaps("superfx/rom/map", 6);
      convertRAM("superfx/ram", 4, "ram", "save.ram", false);
        convertMaps("superfx/ram/map", 6);
  }

  if(auto upd77c25 = oldCartridge["upd77c25"]) {
    string name = upd77c25["program"].text().trimRight(".bin");
    auto dr = mergeMaps(upd77c25.find("dr/map"));
    auto sr = mergeMaps(upd77c25.find("sr/map"));
    uint mask = calcMask(dr[0], sr[0]);
    dr.append(sr);
    dr = mergeMaps(dr);
    newManifest.append("  necdsp model=uPD7725 frequency=8000000\n");
      newManifest.append("    map address=", dr[0]["address"].text(), " mask=0x", hex(mask), "\n");
      newManifest.append("    prom name=", name, ".program.rom size=0x1800\n");
      newManifest.append("    drom name=", name, ".data.rom size=0x800\n");
      newManifest.append("    dram name=", name, ".data.ram size=0x200 volatile\n");
  }

  if(auto msu1 = oldCartridge["msu1"]) {
    newManifest.append("  msu1\n");
      convertMaps("msu1/mmio/map", 4);
      newManifest.append("    rom name=msu1.rom\n");
  }

  string region = oldCartridge["region"].text();
  //string title  = "";
  //string sha256 = "";

  newManifest.append("\ninformation\n");
  if(region) newManifest.append("  region:   ", region, "\n");
  //if(title)  newManifest.append("  title:    ", title, "\n");
  //if(sha256) newManifest.append("  sha256:   ", sha256, "\n");

  return newManifest;
}

auto GamePak::convertManifest_v092_v096(uint targetVersion) -> string {
  auto oldCartridge = document["cartridge"];

  string newManifest = "board";
  if(oldCartridge["region"]) newManifest.append(" region=", oldCartridge["region"].text().downcase());
  newManifest.append("\n");

  auto convertROM = [&](string query, uint indent, string nodeName) -> void {
    if(auto rom = oldCartridge[query]) {
      string romName = rom["name"].text();
      newManifest.append(string::repeat(" ", indent));
      newManifest.append(nodeName, " name=", romName);
      if(rom["size"]) newManifest.append(" size=", rom["size"].text());
      newManifest.append("\n");
    }
  };

  auto convertRAM = [&](string query, uint indent, string nodeName, string ramName) -> void {
    if(auto ram = oldCartridge[query]) {
      bool isVolatile = !ram["name"];
      if(!isVolatile) ramName = ram["name"].text();
      newManifest.append(string::repeat(" ", indent));
      newManifest.append(nodeName, " name=", ramName, " size=", ram["size"].text());
      newManifest.append(isVolatile ? " volatile\n" : "\n");
    }
  };

  auto convertMap = [&](Markup::Node map, uint indent, string suffix = "") -> void {
    newManifest.append(string::repeat(" ", indent));
    newManifest.append("map", suffix, " address=", map["address"].text());
    if(map["size"]) newManifest.append(" size=", map["size"].text());
    if(map["base"]) newManifest.append(" base=", map["base"].text());
    if(map["mask"]) newManifest.append(" mask=", map["mask"].text());
    if(map["select"] && map["select"].natural() != 1) {
      newManifest.append(" mask=0x", hex(map["select"].natural() - 1));
    }
    newManifest.append("\n");
  };

  auto convertMaps = [&](string query, uint indent, string suffix = "") -> void {
    auto maps = oldCartridge.find(query);
    if(!maps) return;
    maps = mergeMaps(maps);
    if(suffix) suffix.prepend("=");
    for(auto& map : maps) {
      if(map["id"].text() == "ram"
      && map["address"].text() == "70-7d,f0-ff:0000-7fff"
      && !map["mask"]) {
        map.append(Markup::Node("mask", "0x8000"));
      }
      if(query == "hitachidsp/map(id=ram)" && !map["mask"]) {
        map.append(Markup::Node("mask", "0x8000"));
      }
      if(query == "necdsp/map(id=ram)" && !map["mask"]) {
        map.append(Markup::Node("mask", "0x8000"));
      }
      if(query == "spc7110/map(id=rom)" && !map["mask"]) {
        map.append(Markup::Node("mask", map["address"].text().beginsWith("c0") ? "0xc00000" : "0x800000"));
      }
      if(query == "obc1/map(id=io)" && !map["mask"]) {
        map.append(Markup::Node("mask", "0xe000"));
      }
      convertMap(map, indent, suffix);
    }
  };

  if(oldCartridge["rom"]) {
    convertROM("rom", 2, "rom");
      convertMaps("map(id=rom)", 4);
  }

  if(oldCartridge["ram"]) {
    convertRAM("ram", 2, "ram", "work.ram");
      convertMaps("map(id=ram)", 4);
  }

  if(auto icd2 = oldCartridge["icd2"]) {
    if(targetVersion >= 107) {
      newManifest.append("  icd revision=", icd2["revision"] ? icd2["revision"].natural() + 1 : 2, "\n");
        convertMaps("icd2/map(id=io)", 4);
        convertROM("icd2/rom", 4, "rom");
      newManifest.append("    gameboy\n");
    } else {
      newManifest.append("  icd2");
      if(icd2["revision"]) newManifest.append(" revision=", icd2["revision"].text());
      newManifest.append("\n");
        convertMaps("icd2/map(id=io)", 4);
        convertROM("icd2/rom", 4, "rom");
    }
  }

  if(auto bsx = oldCartridge["bsx"]) bsx.setName("mcc");  //v092-v094
  if(auto mcc = oldCartridge["mcc"]) {  //v095
    convertRAM("mcc/ram[0]", 2, "ram", "work.ram");
      newManifest.append("    map address=10-1f:5000-5fff mask=0xf000\n");
    newManifest.append("  mcc\n");
      newManifest.append("    map address=00-0f:5000\n");
      newManifest.append("    map=mcu address=00-3f,80-bf:8000-ffff mask=0x408000\n");
      newManifest.append("    map=mcu address=40-7d,c0-ff:0000-ffff\n");
      convertROM("mcc/rom", 4, "rom");
        for(auto map : mcc.find("map(id=rom)")) {
          if(map["address"].text() == "00-3f,80-bf:8000-ffff") {
            if(!map["mask"]) map.append(Markup::Node("mask", "0x408000"));
          }
        }
      convertRAM("mcc/ram[1]", 4, "ram", "download.ram");
        newManifest.append("      map address=00-3f,80-bf:6000-7fff mask=0xe000\n");
      newManifest.append("    bsmemory\n");
  }

  if(auto bsxslot = oldCartridge["bsxslot"]) bsxslot.setName("satellaview");  //v092
  if(auto satellaview = oldCartridge["satellaview"]) {  //v093-v095
    newManifest.append("  bsmemory\n");
      convertMaps("satellaview/map(id=rom)", 4);
  }

  if(auto nodes = oldCartridge.find("sufamiturbo")) {
    if(nodes.size() == 2) {  //v093-v095
      for(uint index : range(2)) {
        newManifest.append("  sufamiturbo\n");
          newManifest.append("    rom\n");
            convertMaps({"sufamiturbo[", index, "]/map(id=rom)"}, 6);
          newManifest.append("    ram\n");
            convertMaps({"sufamiturbo[", index, "]/map(id=ram)"}, 6);
      }
    } else if(nodes(0).find("slot")) {  //v092
      for(string id : {"A", "B"}) {
        newManifest.append("  sufamiturbo\n");
          newManifest.append("    rom\n");
            convertMaps({"sufamiturbo/slot(id=", id, ")/map(id=rom)"}, 6);
          newManifest.append("    ram\n");
            convertMaps({"sufamiturbo/slot(id=", id, ")/map(id=ram)"}, 6);
      }
    }
  }

  if(auto nss = oldCartridge["nss"]) {
    newManifest.append("  nss\n");
      convertMaps("nss/map(id=io)", 4);
      for(auto setting : nss.find("setting")) {
        newManifest.append("    setting name: ", setting["name"].text(), "\n");
        for(auto option : setting.find("option")) {
          newManifest.append("      option value=", option["value"].text(), " name: ", option["name"].text(), "\n");
        }
      }
  }

  if(auto event = oldCartridge["event"]) {
    newManifest.append("  event");
    if(event["name"].text() == "Campus Challenge '92") newManifest.append("=CC92");
    if(event["name"].text() == "Powerfest '94") newManifest.append("=PF94");
    if(event["timer"]) {
      newManifest.append(" timer=");
      auto part = event["timer"].text().split(":");
      if(part.size() == 1) newManifest.append(part(0));
      if(part.size() == 2) newManifest.append(part(0).natural() * 60 + part(1).natural());
    }
    newManifest.append("\n");
      convertMaps("event/map(id=io)", 4);
      uint roms = event.find("rom").size();
      for(uint index : range(roms)) convertROM({"event/rom[", index, "]"}, 4, "rom");
      convertRAM("event/ram", 4, "ram", "work.ram");
        convertMaps("event/map(id=ram)", 6);
  }

  if(auto sa1 = oldCartridge["sa1"]) {
    newManifest.append("  sa1\n");
      convertMaps("sa1/map(id=io)", 4);
      convertROM("sa1/rom", 4, "rom");
        for(auto map : sa1.find("map(id=rom)")) {
          if(map["address"].text() == "00-3f,80-bf:8000-ffff") {
            if(!map["mask"]) map.append(Markup::Node("mask", "0x408000"));
          }
        }
        convertMaps("sa1/map(id=rom)", 6);
      convertRAM("sa1/ram[0]", 4, "bwram", "work.ram");
        for(auto map : sa1.find("map(id=bwram)")) {
          if(map["address"].text() == "00-3f,80-bf:6000-7fff") {
            if(!map["size"]) map.append(Markup::Node("size", "0x2000"));
          }
        }
        convertMaps("sa1/map(id=bwram)", 6);
      convertRAM("sa1/ram[1]", 4, "iram", "internal.ram");
        for(auto map : sa1.find("map(id=iram)")) {
          if(map["address"].text() == "00-3f,80-bf:3000-37ff") {
            if(!map["size"]) map.append(Markup::Node("size", "0x800"));
          }
        }
        convertMaps("sa1/map(id=iram)", 6);
  }

  if(auto superfx = oldCartridge["superfx"]) {
    newManifest.append("  superfx");
    if(superfx["revision"]) newManifest.append(" revision=", superfx["revision"].text());
    newManifest.append("\n");
      convertMaps("superfx/map(id=io)", 4);
      convertROM("superfx/rom", 4, "rom");
        convertMaps("superfx/map(id=rom)", 6);
      convertRAM("superfx/ram", 4, "ram", "work.ram");
        convertMaps("superfx/map(id=ram)", 6);
  }

  if(auto armdsp = oldCartridge["armdsp"]) {
    newManifest.append("  armdsp frequency=", armdsp["frequency"].text(), "\n");
      convertMaps("armdsp/map(id=io)", 4);
      convertROM("armdsp/rom[0]", 4, "prom");
      convertROM("armdsp/rom[1]", 4, "drom");
      convertRAM("armdsp/ram", 4, "dram", armdsp["rom[0]/name"].text().replace(".rom", ".ram"));
  }

  if(auto hitachidsp = oldCartridge["hitachidsp"]) {
    for(auto map : hitachidsp.find("map(id=io)")) {
      if(map["address"].text() == "00-3f,80-bf:6000-7fff") {
        map["address"].setValue("00-3f,80-bf:6c00-6fff,7c00-7fff");
        Markup::Node dmap("map");
        dmap.append(Markup::Node("id", "dram"));
        dmap.append(Markup::Node("address", "00-3f,80-bf:6000-6bff,7000-7bff"));
        dmap.append(Markup::Node("mask", "0xf000"));
        hitachidsp.append(dmap);
      }
    }
    newManifest.append("  hitachidsp model=", hitachidsp["model"].text(), " frequency=", hitachidsp["frequency"].text(), "\n");
      convertMaps("hitachidsp/map(id=io)", 4);
      convertROM("hitachidsp/rom[0]", 4, "rom");
        convertMaps("hitachidsp/map(id=rom)", 6);
      convertRAM("hitachidsp/ram[0]", 4, "ram", "work.ram");
        convertMaps("hitachidsp/map(id=ram)", 6);
      convertROM("hitachidsp/rom[1]", 4, "drom");
      convertRAM("hitachidsp/ram[1]", 4, "dram", hitachidsp["rom[1]/name"].text().replace(".rom", ".ram"));
        convertMaps("hitachidsp/map(id=dram)", 6);
  }

  if(auto necdsp = oldCartridge["necdsp"]) {
    newManifest.append("  necdsp model=", necdsp["model"].text(), " frequency=", necdsp["frequency"].text(), "\n");
      convertMaps("necdsp/map(id=io)", 4);
      convertROM("necdsp/rom[0]", 4, "prom");
      convertROM("necdsp/rom[1]", 4, "drom");
      convertRAM("necdsp/ram", 4, "dram", necdsp["rom[1]/name"].text().replace(".rom", ".ram"));
        for(auto map : necdsp.find("map(id=ram)")) {
          if(map["address"].text() == "68-6f,e8-ef:0000-7fff") {
            if(!map["mask"]) map.append(Markup::Node("mask", "0x8000"));
          }
        }
        convertMaps("necdsp/map(id=ram)", 6);
  }

  if(auto spc7110 = oldCartridge["spc7110"]) {
    newManifest.append("  spc7110\n");
      convertMaps("spc7110/map(id=io)", 4);
      convertMaps("spc7110/map(id=rom)", 4, "mcu");
      convertROM("spc7110/rom[0]", 4, "prom");
      convertROM("spc7110/rom[1]", 4, "drom");
      convertRAM("spc7110/ram", 4, "ram", "work.ram");
        convertMaps("spc7110/map(id=ram)", 6);
  }

  if(auto sdd1 = oldCartridge["sdd1"]) {
    newManifest.append("  sdd1\n");
      for(auto map : sdd1.find("map(id=io)")) {
        if(map["address"].text().endsWith("4800-4807")) {
          map["address"].setValue(map["address"].text().replace("4807", "480f"));
        }
      }
      convertMaps("sdd1/map(id=io)", 4);
      convertROM("sdd1/rom", 4, "rom");
        for(auto map : sdd1.find("map(id=rom)")) {
          if(map["address"].text() == "00-3f,80-bf:8000-ffff") {
            if(map["mask"].natural() == 0x8000) map.remove(map["mask"]);
          }
        }
        convertMaps("sdd1/map(id=rom)", 6);
      convertRAM("sdd1/ram", 4, "ram", "work.ram");
        convertMaps("sdd1/map(id=ram)", 6);
  }

  if(auto obc1 = oldCartridge["obc1"]) {
    newManifest.append("  obc1\n");
      convertMaps("obc1/map(id=io)", 4);
      convertRAM("obc1/ram", 4, "ram", "work.ram");
  }

  if(auto epsonrtc = oldCartridge["epsonrtc"]) {
    newManifest.append("  epsonrtc\n");
      convertMaps("epsonrtc/map(id=io)", 4);
      convertRAM("epsonrtc/ram", 4, "ram", "rtc.ram");
  }

  if(auto sharprtc = oldCartridge["sharprtc"]) {
    newManifest.append("  sharprtc\n");
      convertMaps("sharprtc/map(id=io)", 4);
      convertRAM("sharprtc/ram", 4, "ram", "rtc.ram");
  }

  if(auto msu1 = oldCartridge["msu1"]) {
    newManifest.append("  msu1\n");
      convertMaps("msu1/map(id=io)", 4);
      convertROM("msu1/rom", 4, "rom");
      for(auto track : msu1.find("track")) {
        uint16_t number = track["number"].natural();
        string name = track["name"].text();
        newManifest.append("    track number=", number, " name=", name, "\n");
      }
  }

  bool regionIsSerial = false;
  if(region.match("SHVC-??")     || region.match("SHVC-???\?-JPN")
  || region.match("SHVC-?\?-???") || region.match("SHVC-???\?-???")
  || region.match("SNS-?\?-???")  || region.match("SNS-???\?-???")
  || region.match("SNSP-?\?-???") || region.match("SNSP-???\?-???")
  || region.match("SHVC-SGB") || region.match("SHVC-SGB2-JPN") || region.match("SNS-A-SG-???")) {
    regionIsSerial = true;
  }
  newManifest.append("\ninformation\n");
  if(region && regionIsSerial) newManifest.append("  serial:   ", region, "\n");
  if(board)    newManifest.append("  board:    ", board, "\n");
  if(region && !regionIsSerial) newManifest.append("  region:   ", region, "\n");
  if(revision) newManifest.append("  revision: ", revision, "\n");
  if(name)     newManifest.append("  name:     ", name, "\n");
  if(label)    newManifest.append("  title:    ", label, "\n");
  if(sha256)   newManifest.append("  sha256:   ", sha256, "\n");

  return newManifest;
}
