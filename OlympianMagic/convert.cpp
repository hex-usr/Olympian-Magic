auto GamePak::detectVersionXML() -> VersionRange {
  if(document["cartridge"]) return VersionRange::v073;

  return VersionRange::Unknown;
}

auto GamePak::detectVersionBML() -> VersionRange {
  if(document["game"]) return VersionRange::v107;
  if(document["board"]) return VersionRange::v096_v106;
  if(document["cartridge"]) return VersionRange::v092_v095;
  return VersionRange::Unknown;
}

auto GamePak::convertManifest(uint targetVersion) -> string {
  #define between(low, high)\
    targetVersion >= low && targetVersion <= high
  switch(versionRange) {
  case VersionRange::v073:
    if(between( 96, 106)) return convertManifest_v073_v096(targetVersion);
    if(targetVersion == 107) return convertManifest_v073_v107(targetVersion);
    break;
  case VersionRange::v092_v095:
    if(between( 96, 106)) return convertManifest_v092_v096(targetVersion);
    break;
  case VersionRange::v096_v106:
    if(between( 96, 106)) return markup;
    if(targetVersion == 107) return convertManifest_v096_v107(targetVersion);
    break;
  }
  #undef between

  if(targetVersion >= 107) return convertManifest_v107(targetVersion);

  return "Error: unsupported conversion path";
}

auto GamePak::convertMemories(uint targetVersion) -> vector<Memory> {
  vector<Memory> newMemoryList;
  for(auto memory : memoryList) {
    newMemoryList.append(convertMemory(targetVersion, memory));
  }
  return newMemoryList;
}

auto GamePak::convertMemory(uint targetVersion, Memory memory) -> Memory {
  if(targetVersion >= 107) return convertMemory_v107(memory);

  #define between(low, high)\
    targetVersion >= low && targetVersion <= high
  switch(versionRange) {
  case VersionRange::v073:
    if(between( 92, 106)) return convertMemory_v073_v092(memory);
    break;
  case VersionRange::v092_v095:
    if(between( 92, 106)) return memory;
    break;
  case VersionRange::v096_v106:
    if(between( 92, 106)) return memory;
    break;
  }
  #undef between

  return {};
}

auto GamePak::convertMSU1(uint targetVersion, string name) -> string {
  #define between(low, high)\
    targetVersion >= low && targetVersion <= high
  switch(versionRange) {
  case VersionRange::v073: {
    if(between( 92, 106)) {
      if(name.endsWith(".msu")) {
        return "msu1.rom";
      } else if(name.endsWith(".pcm")) {
        uint trackNumber = toNatural(name.trimLeft({Location::prefix(path), "-"}).trimRight(".pcm"));
        return {"track-", trackNumber, ".pcm"};
      }
    }
    if(targetVersion >= 107) {
      if(name.endsWith(".msu")) {
        return "msu1/data.rom";
      } else if(name.endsWith(".pcm")) {
        uint trackNumber = toNatural(name.trimLeft({Location::prefix(path), "-"}).trimRight(".pcm"));
        return {"msu1/track-", trackNumber, ".pcm"};
      }
    }
    break;
  }
  case VersionRange::v092_v095: {
    if(between( 96, 106)) return name;
    if(targetVersion >= 107) {
      if(name == "msu1.rom") {
        return "msu1/data.rom";
      } else if(name.endsWith(".pcm")) {
        return {"msu1/", name};
      }
    }
    break;
  }
  case VersionRange::v096_v106: {
    if(targetVersion >= 107) {
      if(name == "msu1.rom") {
        return "msu1/data.rom";
      } else if(name.endsWith(".pcm")) {
        return {"msu1/", name};
      }
    }
    break;
  }
  case VersionRange::v107: {
    return name;
  }
  }
  #undef between

  return name;
}
