#include <nall/nall.hpp>
using namespace nall;

#include <ramus/string/markup/xml.hpp>

struct GamePak {
  struct Memory;
  struct Oscillator;

  enum VersionRange : uint {
    Unknown,
    v073,
    v092_v095,
    v096_v106,
    v107,
  };

  auto load(string_view) -> void;
  auto setMarkup(string_view) -> void;
  auto memory(string_view) -> maybe<Memory>;
  auto msu1DataName() const -> string;
  auto msu1TrackName(uint trackID) const -> string;

  //compare.cpp
  auto compare(const string& icarus) -> bool;
  auto compareNodes(Markup::Node nodeM, Markup::Node nodeI) -> bool;

  //convert.cpp
  auto detectVersionXML() -> VersionRange;
  auto detectVersionBML() -> VersionRange;
  auto convertManifest(uint targetVersion) -> string;
  auto convertMemories(uint targetVersion) -> vector<Memory>;
  auto convertMemory(uint targetVersion, Memory memory) -> Memory;
  auto convertMSU1(uint targetVersion, string name) -> string;

  struct Memory {
    auto type() const { return _type; }
    auto size() const { return _size; }
    auto content() const { return _content; }
    auto manufacturer() const { return _manufacturer; }
    auto architecture() const { return _architecture; }
    auto identifier() const { return _identifier; }
    auto isVolatile() const { return _volatile; }
    auto depth() const { return _depth; }
    auto endian() const { return _endian; }
    auto offset() const { return _offset; }
    auto filename() const { return _filename; }
    auto data() const { return _data; }

    auto& type(string type) { _type = type; return *this; }
    auto& size(natural size) { _size = size; return *this; }
    auto& content(string content) { _content = content; return *this; }
    auto& manufacturer(string manufacturer) { _manufacturer = manufacturer; return *this; }
    auto& architecture(string architecture) { _architecture = architecture; return *this; }
    auto& identifier(string identifier) { _identifier = identifier; return *this; }
    auto& setVolatile(boolean newVolatile) { _volatile = newVolatile; return *this; }
    auto& depth(natural depth) { _depth = depth; return *this; }
    auto& endian(Endian endian) { _endian = endian; return *this; }
    auto& offset(natural offset) { _offset = offset; return *this; }
    auto& filename(string filename) { _filename = filename; return *this; }

    auto name() const -> string;

  private:
    string _type;
    natural _size;
    string _content;
    string _manufacturer;
    string _architecture;
    string _identifier;
    boolean _volatile;
    natural _depth = 1;
    Endian _endian = Endian::Unknown;
    natural _offset = 0;
    string _filename;
    vector<uint8_t> _data;
  };

  struct Oscillator {
    auto frequency() { return _frequency; }

    auto& frequency(natural frequency) { _frequency = frequency; return *this; }

  private:
    natural _frequency;
  };

  string path;
  string markup;
  bool icarus;

  Markup::Node document;
  string sha256;
  string label;
  string name;
  string region;
  string revision;
  string board;
  vector<Memory> memoryList;
  vector<Oscillator> oscillatorList;

private:
  VersionRange versionRange;

  //v073.cpp
  auto load_v073() -> void;

  //v092.cpp
  auto load_v092() -> void;
  auto convertMemory_v073_v092(Memory memory) -> Memory;

  //v096.cpp
  auto load_v096() -> void;
  auto convertManifest_v073_v096(uint targetVersion) -> string;
  auto convertManifest_v092_v096(uint targetVersion) -> string;

  //v107.cpp
  auto load_v107() -> void;
  auto convertManifest_v107(uint targetVersion) -> string;
  auto convertManifest_v073_v107(uint targetVersion) -> string;
  auto convertManifest_v096_v107(uint targetVersion) -> string;
  auto convertMemory_v107(Memory memory) -> Memory;

  //utility.cpp
  auto compressAddress(string address) -> string;
  auto mergeMap(const Markup::Node& map0, const Markup::Node& map1) -> maybe<Markup::Node>;
  auto mergeMaps(vector<Markup::Node> maps) -> vector<Markup::Node>;
};
