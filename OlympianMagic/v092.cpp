auto GamePak::load_v092() -> void {
  auto information = document["information"];
  sha256   = information["sha256"].text();
  label    = information["title"].text();
  name     = information["name"].text();

  region   = information["serial"].text().trimLeft("M/").trimRight("-1");
  if(region == "N/A") region = "";
  if(!region && information["region"]) {
    region = information["region"].text();
    if(region == "JP") region = "JPN";
    if(region == "NA") region = "USA";
    if(region == "QC") region = "CAN";
    if(region == "EU") region = "EUR";
  }

  revision = information["revision"].text();
  board    = information["board"].text().
    trimRight("-1").trimRight("-2").
    trimRight("/P10018").trimRight("/P10019");
  if(information["mapper"].text() == "MAD-R") board.append("#R");

  if(revision) {
    if(region.match("SNS*-A-SG-???")) {
      revision.trimLeft("1.");
      auto parts = region.split("-");
      revision = {"SYS-SGB-", revision};
    } else if(region.match("SHVC-??") || region.match("S??*-?*-???")) {
      revision.trimLeft("1.");
      auto parts = region.split("-");
      revision = {parts[0], "-", parts[1], "-", revision};
    }
  }

  auto cartridge = document["cartridge"];

  auto loadMemoryFromNode = [&](string query, string type, string content,
    string manufacturer = "", string architecture = "", string identifier = ""
  ) -> void {
    auto node = cartridge[query];
    if(!node) return;
    string name;
    if(auto leaf = node["name"]) name = leaf.text();
    if(!name) name = string{identifier, ".", content, ".", type}.trimLeft(".").downcase();

    Memory memory;
    memory.type(type);
    if(type == "RAM" || type == "RTC") memory.setVolatile(!node["name"]);
    memory.size(node["size"].natural());
    memory.content(content);
    memory.manufacturer(manufacturer);
    memory.architecture(architecture);
    if(manufacturer && architecture && !identifier) {
      identifier = name.split(".").left().upcase();
      if(identifier == "CX4") identifier = "Cx4";
    }
    if(identifier) memory.identifier(identifier);
    if(manufacturer == "ARM")     memory.depth(content == "Program" || type == "RAM" ? 4 : 1).endian(Endian::LSB);
    if(manufacturer == "Hitachi") memory.depth(3                                            ).endian(Endian::LSB);
    if(manufacturer == "NEC")     memory.depth(content == "Program" ? 3 : 2                 ).endian(Endian::LSB);
    memory.filename(name);
    memoryList.append(memory);
  };

  auto loadOscillatorFromNode = [&](string query) -> void {
    auto node = cartridge[query];
    if(!node) return;

    uint frequency = node["frequency"].natural();

    if(query == "necdsp" && frequency == 8000000) frequency = 7600000;

    if(!frequency) return;
    Oscillator oscillator;
    oscillator.frequency(frequency);
    oscillatorList.append(oscillator);
  };

  loadMemoryFromNode("sa1/rom",    "ROM", "Program");
  loadMemoryFromNode("sa1/ram[0]", "RAM", "Save");
  loadMemoryFromNode("sa1/ram[1]", "RAM", "Internal");

  loadMemoryFromNode("superfx/rom", "ROM", "Program");
  loadMemoryFromNode("superfx/ram", "RAM", "Save");

  loadMemoryFromNode("hitachidsp/rom[0]",  "ROM", "Program");
  loadMemoryFromNode("hitachidsp/ram[0]",  "RAM", "Save");

  loadMemoryFromNode("spc7110/rom[0]", "ROM", "Program");
  loadMemoryFromNode("spc7110/rom[1]", "ROM", "Data");
  loadMemoryFromNode("spc7110/ram",    "RAM", "Save");

  loadMemoryFromNode("sdd1/rom", "ROM", "Program");
  loadMemoryFromNode("sdd1/ram", "RAM", "Save");

  loadMemoryFromNode("obc1/ram", "RAM", "Save");

  loadMemoryFromNode("rom", "ROM", memory("program.rom") ? "Expansion" : "Program");
  loadMemoryFromNode("ram", "RAM", "Save");

  //higan v092 to v095 did not support the Super Game Boy 2.
  loadMemoryFromNode("icd2/rom", "ROM", "Boot", "Nintendo", "DMG", "SGB1");

  loadMemoryFromNode("mcc/ram", "RAM", "Download");

  loadMemoryFromNode("armdsp/rom[0]", "ROM", "Program", "SETA", "ARM6");
  loadMemoryFromNode("armdsp/rom[1]", "ROM", "Data",    "SETA", "ARM6");
  loadMemoryFromNode("armdsp/ram",    "RAM", "Data",    "SETA", "ARM6");

  loadMemoryFromNode("hitachidsp/rom[1]", "ROM", "Data", "Hitachi", "HG51BS169");
  loadMemoryFromNode("hitachidsp/ram[1]", "RAM", "Data", "Hitachi", "HG51BS169");

  loadMemoryFromNode("necdsp/rom[0]", "ROM", "Program", "NEC", cartridge["necdsp/model"].text());
  loadMemoryFromNode("necdsp/rom[1]", "ROM", "Data",    "NEC", cartridge["necdsp/model"].text());
  loadMemoryFromNode("necdsp/ram",    "RAM", "Data",    "NEC", cartridge["necdsp/model"].text());

  loadMemoryFromNode("epsonrtc/ram", "RTC", "Time", "Epson", "RTC4513");

  loadMemoryFromNode("sharprtc/ram", "RTC", "Time", "Sharp", "SRTC");

  if(cartridge["superfx"]) {
    if(!cartridge["superfx/revision"] || cartridge["superfx/revision"].natural() >= 2) {
      oscillatorList.append(Oscillator{}.frequency(21440000));
    }
  }

  loadOscillatorFromNode("armdsp");
  loadOscillatorFromNode("hitachidsp");
  loadOscillatorFromNode("necdsp");
}

auto GamePak::convertMemory_v073_v092(Memory memory) -> Memory {
  string filename = {memory.identifier(), ".", memory.content(), ".", memory.type()};
  filename.trimLeft(".", 1L).downcase();
  memory.filename(filename).offset(0);
  if(memory.depth() > 1) memory.endian(Endian::LSB);
  return memory;
}
