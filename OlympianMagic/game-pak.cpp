#include "game-pak.hpp"

#include "compare.cpp"
#include "convert.cpp"
#include "v073.cpp"
#include "v092.cpp"
#include "v096.cpp"
#include "v107.cpp"
#include "utility.cpp"

auto GamePak::load(string_view _path) -> void {
  path = _path;
  markup = "";
  document.reset();
  memoryList.reset();
  oscillatorList.reset();

  if(file::exists({path, "manifest.bml"})) {
    setMarkup(file::read({path, "manifest.bml"}));
  } else if(file::exists({path, Location::prefix(path), ".xml"})) {
    setMarkup(file::read({path, Location::prefix(path), ".xml"}));
  } else {
    versionRange = VersionRange::v096_v106;

    if(file::exists({path, "sgb.boot.rom"})
    || file::exists({path, "sgb1.boot.rom"})
    || file::exists({path, "sgb2.boot.rom"})
    || file::exists({path, "st018.program.rom"})
    || file::exists({path, "st018.data.rom"})
    || file::exists({path, "cx4.data.rom"})
    || file::exists({path, "dsp1.program.rom"})
    || file::exists({path, "dsp1.data.rom"})
    || file::exists({path, "dsp1b.program.rom"})
    || file::exists({path, "dsp1b.data.rom"})
    || file::exists({path, "dsp2.program.rom"})
    || file::exists({path, "dsp2.data.rom"})
    || file::exists({path, "dsp3.program.rom"})
    || file::exists({path, "dsp3.data.rom"})
    || file::exists({path, "dsp4.program.rom"})
    || file::exists({path, "dsp4.data.rom"})
    || file::exists({path, "st010.program.rom"})
    || file::exists({path, "st010.data.rom"})
    || file::exists({path, "st011.program.rom"})
    || file::exists({path, "st011.data.rom"})
    || file::exists({path, "rtc.ram"})
    ) versionRange = VersionRange::v096_v106;

    /*
    if(file::exists({path, "nintendo.boot.rom"})
    || file::exists({path, "seta.program.rom"})
    || file::exists({path, "seta.data.rom"})
    || file::exists({path, "hitachi.data.rom"})
    || file::exists({path, "nec.program.rom"})
    || file::exists({path, "nec.data.rom"})
    || file::exists({path, "nec.data.ram"})
    || file::exists({path, "time.rtc"})
    ) versionRange = VersionRange::v107;
    */

    /*
    icarus = false;
    if(versionRange >= VersionRange::v096_v106) {
      if(auto result = execute("icarus", "--manifest", path)) {
        icarus = true;
        setMarkup(result.output);
      }
    }
    */
  }
}

auto GamePak::setMarkup(string_view _markup) -> void {
  memoryList.reset();
  oscillatorList.reset();

  markup = _markup;
  bool xml = markup[0] == '<';
  if(xml) {
    document = ramus::XML::unserialize(markup);
    versionRange = detectVersionXML();
  } else {
    document = BML::unserialize(markup);
    versionRange = detectVersionBML();
  }

  if(versionRange == VersionRange::v073) load_v073();
  if(versionRange == VersionRange::v092_v095) load_v092();
  if(versionRange == VersionRange::v096_v106) load_v096();
  if(versionRange == VersionRange::v107) load_v107();
}

auto GamePak::memory(string_view name) -> maybe<Memory> {
  for(auto& memory : memoryList) {
    if(memory.name() == name) return memory;
  }
  return nothing;
}

auto GamePak::Memory::name() const -> string {
  return string{architecture(), ".", content(), ".", type()}.trimLeft(".", 1L).downcase();
}

auto GamePak::msu1DataName() const -> string {
  switch(versionRange) {
  case VersionRange::v073: {
    return string{name, ".msu"};
  }
  case VersionRange::v092_v095:
  case VersionRange::v096_v106: {
    return string{"msu1.rom"};
  }
  case VersionRange::v107: {
    return string{"msu1/data.rom"};
  }
  }
}

auto GamePak::msu1TrackName(uint trackID) const -> string {
  switch(versionRange) {
  case VersionRange::v073: {
    return string{name, "-", trackID, ".pcm"};
  }
  case VersionRange::v092_v095:
  case VersionRange::v096_v106: {
    return string{"track-", trackID, ".pcm"};
  }
  case VersionRange::v107: {
    return string{"msu1/track-", trackID, ".pcm"};
  }
  }
}
