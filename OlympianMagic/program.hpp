#include "game-pak.hpp"

#include <hiro/hiro.hpp>
using namespace hiro;

struct ManifestTab : TabFrameItem {
  ManifestTab(TabFrame*);

  HorizontalLayout layout{this};
    VerticalLayout inLayout{&layout, Size{~0, ~0}};
      TextEdit inManifestEdit{&inLayout, Size{~0, ~0}};
      HorizontalLayout statusLayout{&inLayout, Size{~0, 0}};
        Label statusLabel{&statusLayout, Size{~0, 0}};
      //Button deleteButton{&statusLayout, Size{100, 0}};
    VerticalLayout outLayout{&layout, Size{~0, ~0}};
      TextEdit outManifestEdit{&outLayout, Size{~0, ~0}};
};

struct MemoryTab : TabFrameItem {
  MemoryTab(TabFrame*);

  HorizontalLayout layout{this};
    VerticalLayout inLayout{&layout, Size{~0, ~0}};
      TableView inTable{&inLayout, Size{~0, ~0}};
    VerticalLayout outLayout{&layout, Size{~0, ~0}};
      TableView outTable{&outLayout, Size{~0, ~0}};

  auto refresh(vector<GamePak::Memory> newMemoryList = {}) -> void;
};

struct Program : Window {
  Program(const string in);

  auto load(string path) -> void;
  auto save(string path) -> void;
  auto convert() -> void;

  auto main() -> void;
  auto quit() -> void;

  VerticalLayout layout{this};
    HorizontalLayout loadLayout{&layout, Size{~0, 0}};
      LineEdit loadPath{&loadLayout, Size{~0, 0}};
      Button loadButton{&loadLayout, Size{80, 0}};
    TabFrame panel{&layout, Size{~0, ~0}};
      ManifestTab manifestTab{&panel};
      MemoryTab memoryTab{&panel};
    HorizontalLayout saveLayout{&layout, Size{~0, 0}};
      /*
      #if defined(Hiro_ComboEdit)
      ComboEdit outManifestVersion{&saveLayout, Size{80, 0}};
      #else
      TextEdit outManifestVersion{&saveLayout, Size{80, 0}};
      #endif
      */
      Widget spacer{&saveLayout, Size{~0, 0}};
      Button saveButton{&saveLayout, Size{100, 0}};

  GamePak gamepak;
  Markup::Node settings;
};

extern unique_pointer<Program> program;

auto locate(string name) -> string;
