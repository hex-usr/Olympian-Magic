auto GamePak::load_v107() -> void {
  sha256   = document["game/sha256"].text();
  label    = document["game/label"].text();
  name     = document["game/name"].text();
  region   = document["game/region"].text();
  revision = document["game/revision"].text();
  board    = document["game/board"].text();

  for(auto node : document.find("game/board/memory")) {
    Memory memory;
    memory.type(node["type"].text());
    memory.setVolatile((bool)node["volatile"]);
    memory.size(node["size"].natural());
    memory.content(node["content"].text());
    memory.manufacturer(node["manufacturer"].text());
    memory.architecture(node["architecture"].text());
    memory.identifier(node["identifier"].text());
    if(memory.architecture() == "ARM6") {
      memory
        .depth(memory.content() == "Program" || memory.type() == "RAM" ? 4 : 1)
        .endian(Endian::LSB);
    }
    if(memory.architecture() == "HG51BS169") {
      memory
        .depth(3)
        .endian(Endian::LSB);
    }
    if(memory.architecture() == "uPD7725" || memory.architecture() == "uPD96050") {
      memory
        .depth(memory.content() == "Program" ? 3 : 2)
        .endian(Endian::LSB);
    }
    memory.filename(memory.name());
    memoryList.append(memory);
  }

  for(auto node : document.find("game/board/oscillator")) {
    oscillatorList.append(Oscillator{}.frequency(node["frequency"].natural()));
  }
}

auto GamePak::convertManifest_v107(uint targetVersion) -> string {
  string newManifest = "";
  newManifest.append("game\n");
  if(sha256)   newManifest.append("  sha256:   ", sha256, "\n");
  if(label)    newManifest.append("  label:    ", label, "\n");
  if(name)     newManifest.append("  name:     ", name, "\n");
  if(region)   newManifest.append("  region:   ", region, "\n");
  if(revision) newManifest.append("  revision: ", revision, "\n");
  newManifest.append("  board");
  if(board) newManifest.append(":    ", board);
  newManifest.append("\n");

  for(auto& memory : memoryList) {
    newManifest.append("    memory\n");
    newManifest.append("      type: ", memory.type(), "\n");
    newManifest.append("      size: 0x", hex(memory.size()), "\n");
    newManifest.append("      content: ", memory.content(), "\n");
    if(memory.manufacturer())
    newManifest.append("      manufacturer: ", memory.manufacturer(), "\n");
    if(memory.architecture())
    newManifest.append("      architecture: ", memory.architecture(), "\n");
    if(memory.identifier())
    newManifest.append("      identifier: ", memory.identifier(), "\n");
    if(memory.type() == "RAM" || memory.type() == "RTC") {
      newManifest.append("      ", memory.isVolatile() ? "volatile" : "battery", "\n");
    }
  }

  for(auto& oscillator : oscillatorList) {
    newManifest.append("    oscillator\n");
    newManifest.append("      frequency: ", oscillator.frequency(), "\n");
  }

  return newManifest;
}

auto GamePak::convertManifest_v073_v107(uint targetVersion) -> string {
  auto oldCartridge = document["cartridge"];

  string newManifest = "board\n";

  auto convertMemory = [&](string query, uint indent, string type, string content, string architecture = "") -> void {
    if(auto memory = oldCartridge[query]) {
      newManifest.append(string::repeat(" ", indent));
      newManifest.append("memory type=", type, " content=", content);
      if(architecture) newManifest.append(" architecture=", architecture);
      newManifest.append("\n");
    }
  };

  auto convertMap = [&](Markup::Node map, uint indent) -> void {
    newManifest.append(string::repeat(" ", indent));
    newManifest.append("map address=", map["address"].text());
    uint size = map["size"].text().hex();
    uint base = map["offset"].text().hex();
    uint mask = 0;
    if(size) newManifest.append(" size=0x", hex(size));
    if(base) newManifest.append(" base=0x", hex(base));
    if(map["mask"]) {  //Not supported by bsnes v073, but enabled for overriding the Direct/Linear/Shadow modes
      mask = map["mask"].natural();
    } else {
      string mode = "direct";
      if(map["mode"]) mode = map["mode"].text();
      if(mode == "linear") {
        auto parts = map["address"].text().split(":")[1].split("-");
        mask = ~(parts(0).hex() ^ parts(1).hex()) & 0xffff;
      }
    }
    if(mask) {
      //mask is unnecessary if the size attribute ensures an appropriate mask anyway
      if(map["size"].natural() != 0x10000 - mask) newManifest.append(" mask=0x", hex(mask));
    }
    newManifest.append("\n");
  };

  auto convertMaps = [&](string query, uint indent) -> void {
    auto maps = oldCartridge.find(query);
    if(!maps) return;
    for(auto map : mergeMaps(maps)) convertMap(map, indent);
  };

  auto calcMask = [&](Markup::Node map0, Markup::Node map1) -> uint {
    auto addr0 = map0["address"].text().split(":")[1].split("-");
    auto addr1 = map1["address"].text().split(":")[1].split("-");
    uint mask0 = addr0[0].hex() ^ addr0[1].hex();
    uint mask1 = addr1[0].hex() ^ addr1[1].hex();
    if(mask0 != mask1) return 0;
    return mask0;
  };

  if(oldCartridge["rom"]
  && !oldCartridge["spc7110"]
  && !oldCartridge["sdd1"]
  ) {
    convertMemory("rom", 2, "ROM", /*oldCartridge["spc7110/rom"] ? "Expansion" :*/ "Program");
    convertMaps("rom/map", 4);
  }
  if(oldCartridge["ram"]
  && !oldCartridge["spc7110"]
  && !oldCartridge["obc1"]
  ) {
    convertMemory("ram", 2, "RAM", "Save");
    convertMaps("ram/map", 4);
  }

  if(auto sa1 = oldCartridge["sa1"]) {
    newManifest.append("  processor architecture=W65C816S\n");
      convertMaps("sa1/mmio/map", 4);
      newManifest.append("    mcu\n");
        for(auto map : sa1.find("rom/map")) {
          if(map["address"].text() == "00-3f:8000-ffff"
          || map["address"].text() == "80-bf:8000-ffff") {
            map.append(Markup::Node("mask", "0x408000"));
          }
        }
        convertMaps("sa1/rom/map", 6);
        convertMemory("sa1/rom", 6, "ROM", "Program");
      convertMemory("sa1/bwram", 4, "RAM", "Save");
        for(auto map : sa1.find("bwram/map")) {
          if(map["address"].text() == "00-3f:6000-7fff"
          || map["address"].text() == "80-bf:6000-7fff") {
            if(!map["size"]) map.append(Markup::Node("size", "0x2000"));
          }
        }
        convertMaps("sa1/bwram/map", 6);
      convertMemory("sa1/iram", 4, "RAM", "Internal");
        for(auto map : sa1.find("iram/map")) {
          if(map["address"].text() == "00-3f:3000-37ff"
          || map["address"].text() == "80-bf:3000-37ff") {
            if(!map["size"]) map.append(Markup::Node("size", "0x800"));
          }
        }
        convertMaps("sa1/iram/map", 6);
  }

  if(auto superfx = oldCartridge["superfx"]) {
    newManifest.append("  processor architecture=GSU\n");
      convertMaps("superfx/mmio/map", 4);
      convertMemory("superfx/rom", 4, "ROM", "Program");
        convertMaps("superfx/rom/map", 6);
      convertMemory("superfx/ram", 4, "RAM", "Save");
        convertMaps("superfx/ram/map", 6);
      newManifest.append("    oscillator\n");
  }

  if(auto upd77c25 = oldCartridge["upd77c25"]) {
    string name = upd77c25["program"].text().trimRight(".bin");
    auto dr = mergeMaps(upd77c25.find("dr/map"));
    auto sr = mergeMaps(upd77c25.find("sr/map"));
    uint mask = calcMask(dr[0], sr[0]);
    dr.append(sr);
    dr = mergeMaps(dr);
    newManifest.append("  processor architecture=uPD7725\n");
      newManifest.append("    map address=", dr[0]["address"].text(), " mask=0x", hex(mask), "\n");
      newManifest.append("    memory type=ROM content=Program architecture=uPD7725\n");
      newManifest.append("    memory type=ROM content=Data architecture=uPD7725\n");
      newManifest.append("    memory type=RAM content=Data architecture=uPD7725\n");
      newManifest.append("    oscillator\n");
  }

  if(auto sdd1 = oldCartridge["sdd1"]) {
    newManifest.append("  processor identifier=SDD1\n");
      for(auto map : sdd1.find("mmio/map")) {
        map["address"].setValue(map["address"].text().replace("4807", "480f"));
      }
      convertMaps("sdd1/mmio/map", 4);
      newManifest.append("    mcu\n");
      convertMaps("rom/map", 6);
      convertMaps("sdd1/mcu/map", 6);
      convertMemory("rom", 6, "ROM", "Program");
  }

  if(auto obc1 = oldCartridge["obc1"]) {
    newManifest.append("  processor identifier=OBC1\n");
      for(auto map : obc1.find("mmio/map")) {
        map.append(Markup::Node("mask", "0xe000"));
      }
      convertMaps("obc1/mmio/map", 4);
      convertMaps("ram/map", 4);
      convertMemory("ram", 4, "RAM", "Save");
  }

  return {newManifest, "\n", convertManifest_v107(targetVersion)};
}

auto GamePak::convertManifest_v096_v107(uint targetVersion) -> string {
  auto oldBoard = document["board"];

  string newManifest = "board";
  if(document["information/board"]) newManifest.append(": ", document["information/board"].text());
  newManifest.append("\n");

  auto convertMemory = [&](string query, uint indent, string type, string content, string architecture = "") -> void {
    if(auto memory = oldBoard[query]) {
      newManifest.append(string::repeat(" ", indent));
      newManifest.append("memory type=", type, " content=", content);
      if(architecture) newManifest.append(" architecture=", architecture);
      newManifest.append("\n");
    }
  };

  auto convertMaps = [&](string query, uint indent, bool mcu = false) -> void {
    auto maps = oldBoard.find(query);
    for(auto& map : maps) {
      if( mcu && map.text() != "mcu") continue;
      if(!mcu && map.text() == "mcu") continue;
      newManifest.append(string::repeat(" ", indent));
      newManifest.append("map address=", map["address"].text());
      if(map["size"]) newManifest.append(" size=", map["size"].text());
      if(map["base"]) newManifest.append(" base=", map["base"].text());
      if(map["mask"]) newManifest.append(" mask=", map["mask"].text());
      newManifest.append("\n");
    }
  };

  if(oldBoard["rom"]) {
    convertMemory("rom", 2, "ROM", oldBoard["spc7110/rom"] ? "Expansion" : "Program");
    convertMaps("rom/map", 4);
  }
  if(oldBoard["ram"]) {
    convertMemory("ram", 2, "RAM", "Save");
    convertMaps("ram/map", 4);
  }

  if(auto sa1 = oldBoard["sa1"]) {
    newManifest.append("  processor architecture=W65C816S\n");
      convertMaps("sa1/map", 4);
      newManifest.append("    mcu\n");
        convertMaps("sa1/rom/map", 6);
        convertMemory("sa1/rom", 6, "ROM", "Program");
      convertMemory("sa1/bwram", 4, "RAM", "Save");
        convertMaps("sa1/bwram/map", 6);
      convertMemory("sa1/iram", 4, "RAM", "Internal");
        convertMaps("sa1/iram/map", 6);
  }

  if(auto superfx = oldBoard["superfx"]) {
    newManifest.append("  processor architecture=GSU\n");
      convertMaps("superfx/map", 4);
      convertMemory("superfx/rom", 4, "ROM", "Program");
        convertMaps("superfx/rom/map", 6);
      convertMemory("superfx/ram", 4, "RAM", "Save");
        convertMaps("superfx/ram/map", 6);
      if(!oldBoard["superfx/revision"] || oldBoard["superfx/revision"].natural() >= 2) {
        newManifest.append("    oscillator\n");
      }
  }

  if(auto armdsp = oldBoard["armdsp"]) {
    newManifest.append("  processor architecture=ARM6\n");
      convertMaps("armdsp/map", 4);
      convertMemory("armdsp/prom", 4, "ROM", "Program", "ARM6");
      convertMemory("armdsp/drom", 4, "ROM", "Data",    "ARM6");
      convertMemory("armdsp/ram",  4, "RAM", "Data",    "ARM6");
      newManifest.append("    oscillator\n");
  }

  if(auto hitachidsp = oldBoard["hitachidsp"]) {
    newManifest.append("  processor architecture=HG51BS169\n");
      for(auto& map : oldBoard.find("hitachidsp/map")) {
        if(map["address"].text().beginsWith("70-77:")) continue;
        newManifest.append("    map address=", map["address"].text());
        if(map["size"]) newManifest.append(" size=", map["size"].text());
        if(map["base"]) newManifest.append(" base=", map["base"].text());
        if(map["mask"]) newManifest.append(" mask=", map["mask"].text());
        newManifest.append("\n");
      }
      convertMemory("hitachidsp/rom", 4, "ROM", "Program");
        convertMaps("hitachidsp/rom/map", 6);
      convertMemory("hitachidsp/ram", 4, "RAM", "Save");
        convertMaps("hitachidsp/ram/map", 6);
      convertMemory("hitachidsp/drom", 4, "ROM", "Data", "HG51BS169");
      convertMemory("hitachidsp/dram", 4, "RAM", "Data", "HG51BS169");
        convertMaps("hitachidsp/dram/map", 6);
      newManifest.append("    oscillator\n");
  }

  if(auto necdsp = oldBoard["necdsp"]) {
    newManifest.append("  processor architecture=", necdsp["model"].text(), "\n");
      convertMaps("necdsp/map", 4);
      convertMemory("necdsp/prom", 4, "ROM", "Program", necdsp["model"].text());
      convertMemory("necdsp/drom", 4, "ROM", "Data",    necdsp["model"].text());
      convertMemory("necdsp/dram", 4, "RAM", "Data",    necdsp["model"].text());
        convertMaps("necdsp/dram/map", 6);
      newManifest.append("    oscillator\n");
  }

  if(auto spc7110 = oldBoard["spc7110"]) {
    newManifest.append("  processor identifier=SPC7110\n");
      convertMaps("spc7110/map", 4, false);
      newManifest.append("    mcu\n");
        convertMaps("spc7110/map", 6, true);
        convertMemory("spc7110/prom", 6, "ROM", "Program");
        convertMemory("spc7110/drom", 6, "ROM", "Data");
      convertMemory("spc7110/ram", 4, "RAM", "Save");
        convertMaps("spc7110/ram/map", 6);
  }

  if(auto sdd1 = oldBoard["sdd1"]) {
    convertMemory("sdd1/ram", 2, "RAM", "Save");
      convertMaps("sdd1/ram/map", 4);
    newManifest.append("  processor identifier=SDD1\n");
      convertMaps("sdd1/map", 4);
      newManifest.append("    mcu\n");
        convertMaps("sdd1/rom/map", 6);
        convertMemory("sdd1/rom", 6, "ROM", "Program");
  }

  if(auto obc1 = oldBoard["obc1"]) {
    newManifest.append("  processor identifier=OBC1\n");
      convertMaps("obc1/map", 4);
      convertMemory("obc1/ram", 4, "RAM", "Save");
  }

  if(auto epsonrtc = oldBoard["epsonrtc"]) {
    newManifest.append("  rtc manufacturer=Epson\n");
      convertMaps("epsonrtc/map", 4);
      newManifest.append("    memory type=RTC content=Time manufacturer=Epson\n");
  }

  if(auto sharprtc = oldBoard["sharprtc"]) {
    newManifest.append("  rtc manufacturer=Sharp\n");
      convertMaps("sharprtc/map", 4);
      newManifest.append("    memory type=RTC content=Time manufacturer=Sharp\n");
  }

  return {newManifest, "\n", convertManifest_v107(targetVersion)};
}

auto GamePak::convertMemory_v107(Memory memory) -> Memory {
  memory.filename(memory.name()).offset(0);
  if(memory.depth() > 1) memory.endian(Endian::LSB);
  return memory;
}
