auto GamePak::load_v073() -> void {
  auto cartridge = document["cartridge"];
  label  = Location::prefix(path);
  name   = Location::prefix(path);
  region = cartridge["region"].text();

  auto loadMemoryFromNode = [&](string query, string extension, string type, string content,
    string manufacturer = "", string architecture = "", string identifier = ""
  ) -> void {
    auto node = cartridge[query];
    if(!node) return;
    string name;
    if(auto leaf = node["name"]) name = leaf.text();
    if(!name) name = string{identifier, ".", content, ".", type}.trimLeft(".").downcase();

    Memory memory;
    memory.filename({Location::prefix(path), ".", extension});

    memory.type(type);
    //v073 did not have a way to specify battery vs. volatile.
    if(type == "RAM" || type == "RTC") memory.setVolatile(content != "Save");
    if(node["size"]) {
      memory.size(node["size"].natural());
    } else {
      uint size = file::size({path, memory.filename()});
      memory.size(size);
    }
    memory.content(content);
    memory.manufacturer(manufacturer);
    memory.architecture(architecture);
    if(manufacturer && architecture && !identifier) {
      identifier = name.split(".").left().upcase();
      if(identifier == "CX4") identifier = "Cx4";
    }
    if(identifier) memory.identifier(identifier);
    memoryList.append(memory);
  };

  loadMemoryFromNode("sa1/rom",   "sfc", "ROM", "Program");
  loadMemoryFromNode("sa1/bwram", "srm", "RAM", "Save");
  loadMemoryFromNode("sa1/iram",  "",    "RAM", "Internal");

  loadMemoryFromNode("superfx/rom", "sfc", "ROM", "Program");
  loadMemoryFromNode("superfx/ram", "srm", "RAM", "Save");

  loadMemoryFromNode("rom", "sfc", "ROM", "Program");
  loadMemoryFromNode("ram", "srm", "RAM", "Save");

  if(auto superfx = cartridge["superfx"]) {
    oscillatorList.append(Oscillator{}.frequency(21440000));
  }

  if(auto necdsp = cartridge["upd77c25"]) {
    string identifier = necdsp["program"].text().split(".").left().upcase();
    memoryList.append(Memory{}
      .type("ROM")
      .size(0x1800)
      .content("Program")
      .manufacturer("NEC")
      .architecture("uPD7725")
      .identifier(identifier)
      .depth(3)
      .endian(Endian::MSB)
      .offset(0x0000)
      .filename(necdsp["program"].text())
    );
    memoryList.append(Memory{}
      .type("ROM")
      .size(0x800)
      .content("Data")
      .manufacturer("NEC")
      .architecture("uPD7725")
      .identifier(identifier)
      .depth(2)
      .endian(Endian::MSB)
      .offset(0x1800)
      .filename(necdsp["program"].text())
    );
    memoryList.append(Memory{}
      .type("RAM")
      .size(0x200)
      .content("Data")
      .manufacturer("NEC")
      .architecture("uPD7725")
      .identifier(identifier)
      .setVolatile(true)
      .depth(2)
      .endian(Endian::MSB)
    );
    oscillatorList.append(Oscillator{}.frequency(7600000));
  }
}
